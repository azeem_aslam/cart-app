{

  'use strict';

  angular
    .module('cartAppApp')
    .controller('CartCtrl', CartCtrl);

  function CartCtrl($scope, $log, socket, cartService, MainSvc) {
  	init();
  	$scope.deleteProduct=  deleteProduct;
  	$scope.removeFromCart=  removeFromCart;
  	$scope.checkout=  checkout;

    function init() {
      $scope.cart = [];
      cartService.getProducts().success(function(cart) {
        $scope.cart = cart;
        socket.syncUpdates('cart', $scope.cart);
      });
    }

    function checkout(products){
    	let self = this;
    	products.map( product =>
    		self.deleteProduct(product)
    	);
    }

    function removeFromCart(product){
    	MainSvc.addProduct(product);
    	cartService.deleteProduct(product._id);
    }

    function deleteProduct(product) {
    	cartService.deleteProduct(product._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('cart');
    });
  }

}
