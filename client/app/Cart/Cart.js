(function () {

  'use strict';

  angular
    .module('cartAppApp')
    .config(Config);

  /* @ngInject */
  function Config($stateProvider) {
    $stateProvider
      .state('Cart', {
        url: '/Cart',
        templateUrl: 'app/Cart/Cart.html',
        controller: 'CartCtrl'
      });
  }
  
})();