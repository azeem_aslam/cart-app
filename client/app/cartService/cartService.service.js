{

  'use strict';

  angular
    .module('cartAppApp')
    .service('cartService', CartService);

  /* @ngInject */
  function CartService($http) {
    this.getProducts = getProducts; 
    this.addProduct = addProduct;
    this.deleteProduct = deleteProduct;

    function getProducts() {
      return $http.get('/api/carts');
    }

    function addProduct(product) {
      return $http.post('/api/carts', product);
    }

    function deleteProduct(id) {
      return $http.delete('/api/carts/' + id);
    }
  }

}