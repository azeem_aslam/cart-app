(function() {

  'use strict';

  angular
    .module('cartAppApp')
    .controller('SingleCtrl', SingleCtrl);

  function SingleCtrl($scope, $log, $http, $stateParams, socket) {
	$scope.product = [];

	getProduct($stateParams.id, $http).success(function(product) {
	$scope.product = product;
	socket.syncUpdates('product', $scope.product);
	});

  }

// Services

	function getProduct(id, $http){
		return 	$http.get('/api/products/' + id);
	}
})();
