(function () {

  'use strict';

  angular
    .module('cartAppApp')
    .config(Config);

  /* @ngInject */
  function Config($stateProvider) {
    $stateProvider
      .state('single', {
        url: '/:id',
        templateUrl: 'app/single/single.html',
        controller: 'SingleCtrl'
      });
  }
  
})();