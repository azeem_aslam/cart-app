{

  'use strict';

  angular
    .module('cartAppApp')
    .service('MainSvc', MainSvc);

  /* @ngInject */
  function MainSvc($http) {
    this.getProducts = getProducts; 
    this.addProduct = addProduct;
    this.deleteProduct = deleteProduct;

    function getProducts() {
      return $http.get('/api/products');
    }

    function addProduct(product) {
      return $http.post('/api/products', product);
    }

    function deleteProduct(id) {
      return $http.delete('/api/products/' + id);
    }
  }

}