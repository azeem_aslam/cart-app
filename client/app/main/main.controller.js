{

  'use strict';

  angular
    .module('cartAppApp')
    .controller('MainCtrl', MainCtrl);

  /* @ngInject */
  function MainCtrl($scope, $http, socket, MainSvc, cartService) {
    
    $scope.addProduct = addProduct;
    $scope.deleteProduct=  deleteProduct;
    $scope.addToCart=  addToCart;

    {
      $scope.awesomeProducts = [];
      MainSvc.getProducts().success(awesomeProducts => {
        $scope.awesomeProducts = awesomeProducts;
        socket.syncUpdates('product', $scope.awesomeProducts);
      });
    }
    
    function addProduct() {
      if($scope.newProduct === '') {
        return;
      }
      MainSvc.addProduct({ name: $scope.newProduct });
      $scope.newProduct = '';
    };

    function addToCart(product) {
      cartService.addProduct(product);
      this.deleteProduct(product);
    };

    function deleteProduct(product) {
      MainSvc.deleteProduct(product._id);
    };

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('product');
    });
  }

}