# MEAN Stack CART-App #


# Usage #
Install 'yo grunt-cli gulp-cli bower':

npm install -g yo grunt-cli gulp-cli bower

Run grunt for building, grunt serve for preview, and grunt serve:dist for a preview of the built app.

### Prerequisites ###
MongoDB - Download and Install MongoDB - If you plan on scaffolding your project with mongoose, you'll need mongoDB to be installed and have the mongod process running.