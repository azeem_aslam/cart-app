'use strict';

var _ = require('lodash');
var Q = require('q');
var Product = require('./product.model');

exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

// Get list of product
function index() {
  var deferred = Q.defer();

  Product.find({}, function (err, products) {
    if(err) return deferred.reject(err);
     deferred.resolve(products);
  });
  return deferred.promise;
};

// Get a single product
function show(id) {
  var deferred = Q.defer();

  Product.findById(id, function (err, product) {
    if(err) return deferred.reject(err);
    if (!product) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'Product: ' + id + ' is not found.'
      })
    );
    deferred.resolve(product);
  });
  return deferred.promise;
};

// Creates a new product in the DB.
function create(params) {
  var deferred = Q.defer();

  Product.create(params, function (err, product) {
    if(err) return deferred.reject(err);
    deferred.resolve(product);
  });
  return deferred.promise;
};

// Updates an existing product in the DB.
function update(id, params) {
  var deferred = Q.defer();

  Product.findById(id, function (err, product) {
    if(err) return deferred.reject(err);
    if (!product) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'Product: ' + id + ' is not found.'
      })
    );

    var updated = _.merge(product, params);
    updated.save(function (err) {
      if (err) { return deferred.reject(err); }
      return deferred.resolve(product);
    }); 
  });
  return deferred.promise;
};

// Deletes a product from the DB.
function destroy(id) {
  var deferred = Q.defer();

  Product.findById(id, function (err, product) {
    if(err) return deferred.reject(err);
    if (!product) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'Product: ' + id + ' is not found.'
      })
    );

    product.remove(function(err) {
      if(err) { return deferred.reject(err); }
      return deferred.resolve(204);
    });
    
  });
  return deferred.promise;
};