'use strict';

var ProductService = require('./product.service');

exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

// Get list of product
function index(req, res) {
  ProductService
    .index()

    .then(function(products) {
      res.json(200, products);
    })
    .catch(function(err) {
      res.send(500, err);
    });
};

// Get a single product
function show(req, res) {
  ProductService
    .show(req.params.id)

    .then(function(product) {
      res.json(product);
    })
    .catch(function(err) {
      if(err.code === 'NOT_FOUND') {
        return res.send(404);
      } 
      res.send(500, err);
    });
};

// Creates a new product in the DB.
function create(req, res) {
  ProductService
    .create(req.body)

    .then(function(product) {
      res.json(201, product);
    })
    .catch(function(err) {
      res.send(500, err);
    });
};

// Updates an existing product in the DB.
function update(req, res) {
  if(req.body._id) { delete req.body._id; }

  ProductService
    .update(req.params.id, req.body)

    .then(function(product) {
      res.json(201, product);
    })
    .catch(function(err) {
      if(err.code === 'NOT_FOUND') {
        return res.send(404);
      } 
      res.send(500, err);
    });
};

// Deletes a product from the DB.
function destroy(req, res) {
  ProductService
    .destroy(req.params.id)

    .then(function(product) {
      res.send(204);
    })
    .catch(function(err) {
      if(err.code === 'NOT_FOUND') {
        return res.send(404);
      } 
      res.send(500, err);
    });
};
