'use strict';

var CartService = require('./cart.service');

exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

// Get list of cart
function index(req, res) {
  CartService
    .index()

    .then(function(carts) {
      res.json(200, carts);
    })
    .catch(function(err) {
      res.send(500, err);
    });
};

// Get a single cart
function show(req, res) {
  CartService
    .show(req.params.id)

    .then(function(cart) {
      res.json(cart);
    })
    .catch(function(err) {
      if(err.code === 'NOT_FOUND') {
        return res.send(404);
      } 
      res.send(500, err);
    });
};

// Creates a new cart in the DB.
function create(req, res) {
  CartService
    .create(req.body)

    .then(function(cart) {
      res.json(201, cart);
    })
    .catch(function(err) {
      res.send(500, err);
    });
};

// Updates an existing cart in the DB.
function update(req, res) {
  if(req.body._id) { delete req.body._id; }

  CartService
    .update(req.params.id, req.body)

    .then(function(cart) {
      res.json(201, cart);
    })
    .catch(function(err) {
      if(err.code === 'NOT_FOUND') {
        return res.send(404);
      } 
      res.send(500, err);
    });
};

// Deletes a cart from the DB.
function destroy(req, res) {
  CartService
    .destroy(req.params.id)

    .then(function(cart) {
      res.send(204);
    })
    .catch(function(err) {
      if(err.code === 'NOT_FOUND') {
        return res.send(404);
      } 
      res.send(500, err);
    });
};
