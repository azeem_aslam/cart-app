'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CartSchema = new Schema({
  name: String,
  info: String
});

module.exports = mongoose.model('Cart', CartSchema);