'use strict';

var _ = require('lodash');
var Q = require('q');
var Cart = require('./cart.model');

exports.index = index;
exports.show = show;
exports.create = create;
exports.update = update;
exports.destroy = destroy;

// Get list of cart
function index() {
  var deferred = Q.defer();

  Cart.find({}, function (err, carts) {
    if(err) return deferred.reject(err);
     deferred.resolve(carts);
  });
  return deferred.promise;
};

// Get a single cart
function show(id) {
  var deferred = Q.defer();

  Cart.findById(id, function (err, cart) {
    if(err) return deferred.reject(err);
    if (!cart) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'Cart: ' + id + ' is not found.'
      })
    );
    deferred.resolve(cart);
  });
  return deferred.promise;
};

// Creates a new cart in the DB.
function create(params) {
  var deferred = Q.defer();

  Cart.create(params, function (err, cart) {
    if(err) return deferred.reject(err);
    deferred.resolve(cart);
  });
  return deferred.promise;
};

// Updates an existing cart in the DB.
function update(id, params) {
  var deferred = Q.defer();

  Cart.findById(id, function (err, cart) {
    if(err) return deferred.reject(err);
    if (!cart) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'Cart: ' + id + ' is not found.'
      })
    );

    var updated = _.merge(cart, params);
    updated.save(function (err) {
      if (err) { return deferred.reject(err); }
      return deferred.resolve(cart);
    }); 
  });
  return deferred.promise;
};

// Deletes a cart from the DB.
function destroy(id) {
  var deferred = Q.defer();

  Cart.findById(id, function (err, cart) {
    if(err) return deferred.reject(err);
    if (!cart) return deferred.reject(
      Error.new({
        code: 'NOT_FOUND',
        message: 'Cart: ' + id + ' is not found.'
      })
    );

    cart.remove(function(err) {
      if(err) { return deferred.reject(err); }
      return deferred.resolve(204);
    });
    
  });
  return deferred.promise;
};